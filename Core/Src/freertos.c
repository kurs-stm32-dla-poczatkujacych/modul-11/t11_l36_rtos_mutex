/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include "printf.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for Led2Task */
osThreadId_t Led2TaskHandle;
const osThreadAttr_t Led2Task_attributes = {
  .name = "Led2Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for Led3Task */
osThreadId_t Led3TaskHandle;
const osThreadAttr_t Led3Task_attributes = {
  .name = "Led3Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for Led4Task */
osThreadId_t Led4TaskHandle;
const osThreadAttr_t Led4Task_attributes = {
  .name = "Led4Task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for MutexPrintf */
osMutexId_t MutexPrintfHandle;
const osMutexAttr_t MutexPrintf_attributes = {
  .name = "MutexPrintf"
};
/* Definitions for MutexOther */
osMutexId_t MutexOtherHandle;
const osMutexAttr_t MutexOther_attributes = {
  .name = "MutexOther"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartLed2Task(void *argument);
void StartLed3Task(void *argument);
void StartLed4Task(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of MutexPrintf */
  MutexPrintfHandle = osMutexNew(&MutexPrintf_attributes);

  /* creation of MutexOther */
  MutexOtherHandle = osMutexNew(&MutexOther_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of Led2Task */
  Led2TaskHandle = osThreadNew(StartLed2Task, NULL, &Led2Task_attributes);

  /* creation of Led3Task */
  Led3TaskHandle = osThreadNew(StartLed3Task, NULL, &Led3Task_attributes);

  /* creation of Led4Task */
  Led4TaskHandle = osThreadNew(StartLed4Task, NULL, &Led4Task_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartLed2Task */
/**
  * @brief  Function implementing the Led2Task thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartLed2Task */
void StartLed2Task(void *argument)
{
  /* USER CODE BEGIN StartLed2Task */
  /* Infinite loop */
  for(;;)
  {
	  osMutexAcquire(MutexOtherHandle, osWaitForever);
	  printf("Led2 Mutex taken\n\r");

	  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	  printf("Led2\n\r");

	  osMutexRelease(MutexOtherHandle);
	  printf("Led2 Mutex released\n\r");

	  osDelay(500);
  }
  /* USER CODE END StartLed2Task */
}

/* USER CODE BEGIN Header_StartLed3Task */
/**
* @brief Function implementing the Led3Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartLed3Task */
void StartLed3Task(void *argument)
{
  /* USER CODE BEGIN StartLed3Task */
  /* Infinite loop */
  for(;;)
  {
	  osMutexAcquire(MutexOtherHandle, osWaitForever);
	  printf("Led3 Mutex taken\n\r");

	  HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	  printf("Led3\n\r");

	  osMutexRelease(MutexOtherHandle);
	  printf("Led3 Mutex released\n\r");

	  osDelay(300);
  }
  /* USER CODE END StartLed3Task */
}

/* USER CODE BEGIN Header_StartLed4Task */
/**
* @brief Function implementing the Led4Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartLed4Task */
void StartLed4Task(void *argument)
{
  /* USER CODE BEGIN StartLed4Task */
  /* Infinite loop */
  for(;;)
  {
	  osMutexAcquire(MutexOtherHandle, osWaitForever);
	  printf("Led4 Mutex taken\n\r");

	  HAL_GPIO_TogglePin(LD4_GPIO_Port, LD4_Pin);
	  printf("Led4\n\r");
	  osMutexRelease(MutexOtherHandle);
	  printf("Led4 Mutex released\n\r");

	  osDelay(1000);
  }
  /* USER CODE END StartLed4Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void _putchar(char character)
{
  // send char to console etc.
	osMutexAcquire(MutexPrintfHandle, osWaitForever);
	HAL_UART_Transmit(&huart2, (uint8_t*)&character, 1, 1000);
	osMutexRelease(MutexPrintfHandle);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
